# About
Terminal based pranayama timer. Eat your heart out Mr Crowley. 

# Installation

```shell
git clone https://gitlab.com/binrc/pt ~/src/pt
cd ~/src/pt && make
```

# Usage

```
-i	inhale time, default is 10s
-e	exhale time, default is 20s
-h	hold breath time, default is 0s
-c	number of cycles, defualt = 0 = go indefinitely 
-p	audio player, default is mplayer
-m	metronome sound
-b	beep sound
--dump	dump all the variables
--dmax	really dump all the variables
--help	print this text
```

## example

```shell
./pt -i 10 -h 30 -e 20 -c 30
```
