#!SHEBANG
#boilerplate
import os
import sys
import time
import signal
import datetime
argc = len(sys.argv)
argv = sys.argv

#vars
itime= None
otime= None
htime= None
cycles= None
player= None
tick= None
beep= None
dump= 0

labels= [
	"InhaleTime",
	"ExhaleTime",
	"HoldTime",
	"TotalCycles",
	"AudioPlayer",
	"ClickSound",
	"BeepSound"
]

flags= [
	["-i",itime, 10],
	["-e",otime, 20],
	["-h",htime, 0],
	["-c",cycles, 0],
	["-p",player, "mplayer"],
	["-m",tick, "metronome.ogg"],
	["-b",beep, "beep.ogg"]
]


#functs 
def help():
	print("usage:")
	print("-i\tinhale time, default is 10s")
	print("-e\texhale time, default is 20s")
	print("-h\thold breath time, default is 0s")
	print("-c\tnumber of cycles, defualt = 0 = go indefinitely ")
	print("-p\taudio player, default is mplayer")
	print("-m\tmetronome sound")
	print("-b\tbeep sound")
	print("--dump\tdump all the variables")
	print("--dmax\treally dump all the variables")
	print("--help\tprint this text")
	print("\n")

def playa(player, file):
	os.system(player+ " " + file + " >/dev/null &") # run in bg so C-c won't fucking catch

def shittyDebug(labels, flags, level):
	for i in range(0, level - 1):
		print(labels[i]+ "\t" + str(flags[i][1]))

	if dlev > len(flags):
		print("itime= " + str(itime))
		print("otime= " + str(otime))
		print("htime= " + str(htime))
		print("cycles= " + str(cycles))

def results(flags, labels, dlev, performedCycles, curlen):
	print('\n\n')
	print("Cycles\t\t",performedCycles)
	print("TotalTime\t", str(datetime.timedelta(seconds=curlen)))
	shittyDebug(labels,flags, dlev)
	exit(0)

#C-c handler
def handler(signum, frame):
	results(flags, labels, dlev, performedCycles, curlen)

signal.signal(signal.SIGINT,handler)

#input handler
for i in range(0, argc):
	if argv[i] == "--help":
		help()

	if argv[i] == "--dump":
		dump = 1

	if argv[i] == "--dmax":
		dump = 2

	for j in range(0, len(flags)):
		if argv[i] == flags[j][0]:
			flags[j][1] = argv[i+1]

	i = i + 1

#set defaults
for i in range(0, len(flags)):
	if not flags[i][1]:
		flags[i][1]= flags[i][2]

#determine dump level
if dump == 1:
	dlev= len(flags)
elif dump == 2:
	dlev= len(flags) + 1
else:
	dlev= 4

#update vars
itime = int(flags[0][1])
otime = int(flags[1][1])
htime = int(flags[2][1])
cycles= int(flags[3][1])
player= flags[4][1]
tick  = flags[5][1]
beep  = flags[6][1]


#main loop
cycleLen = itime + htime + otime
performedCycles = 0
curlen = 0
s = 0;

if cycles == 0:
	cycles = sys.maxsize

if htime > 0:
	metProg = [[itime, "inhale"], [otime, "exhale"], [htime, "hold"]]
else:
	metProg = [[itime, "inhale"], [otime, "exhale"]]

while performedCycles < cycles: 
	i = 0
	for i in range(0, len(metProg)):
		s = 0
		while s < metProg[i][0]:
			s += 1
			curlen +=1
			print("\r                        \r" + metProg[i][1] + ": " + str(s) + "/" + str(metProg[i][0]),end='') # ansi magic
			playa(player,tick)
			time.sleep(1)

	
	
	performedCycles += 1


#print results
playa(player, beep)
results(flags,labels, dlev, performedCycles, curlen)
