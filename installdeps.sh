#!/bin/sh
os=$(uname)

linux() {
	which dnf

	if [ $? -eq 0 ]; then
		dnf install python mplayer
		exit 0
	else
		which apt
	fi

	
	if [ $? -eq 0 ]; then
		apt install python mplayer
		exit 0
	else
		echo "get a different distro"
	fi


	exit 1

}

if [ $os = "FreeBSD" ]; then
	pkg install python mplayer
	exit 0
fi

if [ $os == "OpenBSD" ]; then
	pkg_add python mplayer
	exit 0 
fi

if [ $os == "NetBSD" ]; then
	pkgin python38 mplayer
	exit 0
fi

if [ $os == "Linux" ]; then
	linux
fi
	
echo "unknown OS"
