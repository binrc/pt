#!/bin/sh

py=$(which python)

if [ -z $py ]; then
	py=$(which python3)
fi

if [ -z $py ]; then
	py=$(which python3.8)
fi

if [ -z $py ]; then
	py=$(which python3.10)
fi

if [ -z $py ]; then
	echo "no python"
fi


cat main.py | sed "s,SHEBANG,$py,g" > pt
exit
